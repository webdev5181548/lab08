import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const rolesRepository = AppDataSource.getRepository(Role);
    const adminRole = await rolesRepository.findOneBy({ id: 1 });
    const userRole = await rolesRepository.findOneBy({ id: 2 });

    const usersRepository = AppDataSource.getRepository(User);
    await usersRepository.clear();
    console.log("Inserting a new user into the memory...");
    var user = new User();
    user.id = 1;
    user.email = "admin@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await usersRepository.save(user);

    // const admin = await userRepository.findOneBy({ id: 1 });
    // console.log(admin);

    user = new User();
    user.id = 2;
    user.email = "user1@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await usersRepository.save(user);

    // const user1 = await userRepository.findOneBy({ id: 2 });
    // console.log(user1);

    user = new User();
    user.id = 3;
    user.email = "user2@email.com";
    user.password = "Pass@1234";
    user.gender = "female";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the database...");
    await usersRepository.save(user);

    // const user2 = await userRepository.findOneBy({ id: 3 });
    // console.log(user2);

    const users = await usersRepository.find({ relations: { roles: true } });
    console.log(JSON.stringify(users, null, 2));

    const roles = await rolesRepository.find({ relations: { users: true } });
    console.log(JSON.stringify(roles, null, 2));
  })
  .catch((error) => console.log(error));
